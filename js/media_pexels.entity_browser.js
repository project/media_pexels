/**
 * @file pexels.entity_browser.js
 */
(function (Drupal, $, once) {

  'use strict';

  /**
   * Registers behaviours related to eb widget.
   */

  Drupal.behaviors.pexelsEntityBrowser = {
    attach: function (context) {
      $(once('media-pexels-entity-browser-click', '.grid-item', context)).click(function () {
        var input = $(this).find('.views-field-entity-browser-select input');
        input.prop('checked', !input.prop('checked'));
        if (input.prop('checked')) {
          $(this).addClass('checked');
          var render = $(this).find('.grid-item');
          $(render).css('opacity', 0.3);
        }
        else {
          $(this).removeClass('checked');
          var render = $(this).find('.grid-item');
          $(render).css('opacity', 1);
        }
      });
      $(once('pexel-enter-submit', '.media-pexels-search', context)).find('input[type="search"]').each(function () {
        $(this).on('keypress', function (event) {
          if (event.keyCode == 13) {
            event.preventDefault();
            $('.media-pexels-search').find('input[type="submit"]').first().click();
          }
        });
      });
    }
  };

} (Drupal, jQuery, once));
