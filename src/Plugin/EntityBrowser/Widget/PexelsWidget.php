<?php

namespace Drupal\media_pexels\Plugin\EntityBrowser\Widget;

use Drupal\Component\Utility\NestedArray;
use Drupal\Component\Utility\Unicode;
use Drupal\Component\Utility\UrlHelper;
use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityFieldManagerInterface;
use Drupal\Core\File\Exception\FileException;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Logger\LoggerChannelFactoryInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\Core\Utility\Token;
use Drupal\entity_browser\Element\EntityBrowserPagerElement;
use Drupal\entity_browser\WidgetBase;
use Drupal\media_pexels\Events\Events;
use Drupal\media_pexels\Events\PexelsMediaEntityCreateEvent;
use Drupal\media_pexels\PexelsClient;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides an Entity Browser widget for Pexels.
 *
 * @EntityBrowserWidget(
 *   id = "media_pexels",
 *   label = @Translation("Pexels entity browser"),
 *   description = @Translation("Used to browse and use Pexels images."),
 * )
 */
class PexelsWidget extends WidgetBase {

  /**
   * The pexels client.
   *
   * @var \Drupal\media_pexels\PexelsClient
   */
  protected $pexelsClient;

  /**
   * Results per page default value.
   *
   * @var int
   */
  protected $defaultPerPage = 15;

  /**
   * Current user.
   *
   * @var \Drupal\Core\Session\AccountInterface
   */
  protected $currentUser;

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected $fileSystem;

  /**
   * Entity field manager.
   *
   * @var \Drupal\Core\Entity\EntityFieldManagerInterface
   */
  protected $entityFieldManager;

  /**
   * The token service.
   *
   * @var \Drupal\Core\Utility\Token|\Drupal\token\Token
   */
  protected $token;

  /**
   * Logger factory service.
   *
   * @var \Drupal\Core\Logger\LoggerChannelFactoryInterface
   */
  protected $loggerFactory;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $widget = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $widget->setCurrentUser($container->get('current_user'));
    $widget->setPexelsClient($container->get('media_pexels.pexels_client'));
    $widget->setFileSystem($container->get('file_system'));
    $widget->setToken($container->get('token'));
    $widget->setEntityFieldManager($container->get('entity_field.manager'));
    $widget->setLogger($container->get('logger.factory'));

    return $widget;
  }

  /**
   * Set the pexels client service.
   *
   * @param \Drupal\media_pexels\PexelsClient $pexelsClient
   *   The pexels client.
   */
  protected function setPexelsClient(PexelsClient $pexelsClient) {
    $this->pexelsClient = $pexelsClient;
  }

  /**
   * Set the current user service.
   *
   * @param \Drupal\Core\Session\AccountInterface $currentUser
   *   The current user service.
   */
  protected function setCurrentUser(AccountInterface $currentUser) {
    $this->currentUser = $currentUser;
  }

  /**
   * Set the token service.
   *
   * @param \Drupal\Core\Utility\Token $token
   *   The token service.
   */
  protected function setToken(Token $token) {
    $this->token = $token;
  }

  /**
   * Set the filesystem service.
   *
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The filesystem service.
   */
  protected function setFileSystem(FileSystemInterface $fileSystem) {
    $this->fileSystem = $fileSystem;
  }

  /**
   * Set entity field manager service.
   *
   * @param \Drupal\Core\Entity\EntityFieldManagerInterface $entityFieldManager
   *   The field manager service.
   */
  protected function setEntityFieldManager(EntityFieldManagerInterface $entityFieldManager) {
    $this->entityFieldManager = $entityFieldManager;
  }

  /**
   * Set logger.
   *
   * @param \Drupal\Core\Logger\LoggerChannelFactoryInterface $logger_factory
   *   Logger factory.
   */
  public function setLogger(LoggerChannelFactoryInterface  $logger_factory) {
    $this->loggerFactory = $logger_factory;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'api_key' => NULL,
      'media_type' => NULL,
      'name_field' => NULL,
      'license_field' => NULL,
      'upload_location' => 'public://media_pexels/[date:custom:Y]-[date:custom:m]',
      'download_size' => 'download_size',
      'custom_size_height' => NULL,
      'custom_size_width' => NULL,
    ] + parent::defaultConfiguration();

  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildConfigurationForm($form, $form_state);
    $wrapper_class = 'media-pexels-form-ajax-wrapper-' . $this->uuid();
    $form['#attributes']['class'][] = $wrapper_class;

    $form['api_key'] = [
      '#type' => 'textfield',
      '#title' => $this->t('API key'),
      '#required' => TRUE,
      '#description' => $this->t('The api kay associated with your Pexels account.'),
      '#default_value' => $this->configuration['api_key'],
    ];

    $media_type_options = [];
    foreach ($this->entityTypeManager->getStorage('media_type')->loadMultiple() as $media_type) {
      $media_type_options[$media_type->id()] = $media_type->label();
    }

    $ajax_wrapper = 'media-pexels-ajax-wrapper-' . $this->uuid();

    $form['media_type_container'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => $ajax_wrapper,
      ],
    ];

    $form['media_type_container']['media_type'] = [
      '#type' => 'select',
      '#title' => $this->t('Media type'),
      '#options' => $media_type_options,
      '#required' => TRUE,
      '#description' => $this->t('Which media type to create.'),
      '#default_value' => $this->configuration['media_type'],
      '#ajax' => [
        'callback' => [$this, 'mediaTypeSelectChange'],
        'event' => 'change',
        'wrapper' => $ajax_wrapper,
      ],
    ];

    $user_input = $form_state->getUserInput();
    $media_type_user_input = NestedArray::getValue($user_input, ['table', $this->uuid(), 'form', 'media_type_container', 'media_type']);
    $selected_media_type = $media_type_user_input ?? $this->configuration['media_type'];

    $textfield_options = [];
    if ($selected_media_type) {
      $media_type_fields = $this->entityFieldManager->getFieldDefinitions('media', $selected_media_type);
      foreach ($media_type_fields as $media_type_field) {
        if ($media_type_field->getType() == 'string') {
          $textfield_options[$media_type_field->getName()] = $media_type_field->getLabel();
        }
      }
    }

    $linkfield_options = [];
    if ($selected_media_type) {
      $media_type_fields = $this->entityFieldManager->getFieldDefinitions('media', $selected_media_type);
      foreach ($media_type_fields as $media_type_field) {
        if ($media_type_field->getType() == 'link') {
          $linkfield_options[$media_type_field->getName()] = $media_type_field->getLabel();
        }
      }
    }

    $form['media_type_container']['name_field'] = [
      '#type' => 'select',
      '#title' => $this->t('Name field'),
      '#options' => $textfield_options,
      '#required' => TRUE,
      '#description' => $this->t('Which field to use to store the title for this media.'),
      '#default_value' => $this->configuration['name_field'],
    ];

    $form['media_type_container']['license_field'] = [
      '#type' => 'select',
      '#title' => $this->t('License link field'),
      '#options' => $linkfield_options,
      '#description' => $this->t('Which field to use to store the license link.'),
      '#default_value' => $this->configuration['license_field'],
      '#empty_option' => $this->t('-None-'),
    ];

    $form['upload_location'] = [
      '#type' => 'textfield',
      '#required' => TRUE,
      '#title' => $this->t('Upload location'),
      '#default_value' => $this->configuration['upload_location'],
      '#description' => $this->t('Tokens will be replaced'),
    ];

    $form['download_size'] = [
      '#type' => 'select',
      '#title' => $this->t('Download size'),
      '#options' => [
        'original' => 'original',
        'large2x' => 'large2x',
        'large' => 'large',
        'medium' => 'medium',
        'small' => 'small',
        'portrait' => 'portrait',
        'landscape' => 'landscape',
        'tiny' => 'tiny',
        'custom' => 'custom',
      ],
      '#required' => TRUE,
      '#description' => $this->t('Which image size to download.'),
      '#default_value' => $this->configuration['download_size'],
    ];

    $form['custom_size_width'] = [
      '#type' => 'number',
      '#title' => $this->t('Custom width'),
      '#default_value' => $this->configuration['custom_size_width'],
      '#states' => [
        'visible' => [
          ':input[name="table[' . $this->uuid() . '][form][download_size]"]' => ['value' => 'custom'],
        ],
      ]
    ];

    $form['custom_size_height'] = [
      '#type' => 'number',
      '#title' => $this->t('Custom height'),
      '#default_value' => $this->configuration['custom_size_height'],
      '#states' => [
        'visible' => [
          ':input[name="table[' . $this->uuid() . '][form][download_size]"]' => ['value' => 'custom'],
        ],
      ]
    ];

    return $form;
  }

  /**
   * Media type select ajax callback.
   *
   * @param array $form
   *   The form.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   *
   * @return array
   *   The form array.
   */
  public function mediaTypeSelectChange(array $form, FormStateInterface $form_state) {
    return $form['widgets']['table'][$this->uuid()]['form']['media_type_container'];
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state) {
    $values = $form_state->getValues();
    $api_key = NestedArray::getValue($values, ['table', $this->uuid(), 'form', 'api_key']);
    try {
      if (!$this->pexelsClient->checkKey($api_key)) {
        $form_state->setError($form['widgets']['table'][$this->uuid()]['form']['api_key'], $this->t('The api key is not valid'));
      }

      $name_field = NestedArray::getValue($values, ['table', $this->uuid(), 'form', 'media_type_container', 'name_field']);
      $license_field = NestedArray::getValue($values, ['table', $this->uuid(), 'form', 'media_type_container', 'license_field']);
      if ($name_field == $license_field) {
        $form_state->setError($form['widgets']['table'][$this->uuid()]['form']['media_type_container'], $this->t('Target fields for license and name cannot be identical'));
      }

      if (NestedArray::getValue($values, ['table', $this->uuid(), 'form', 'download_size']) == 'custom') {
        $custom_size_width = NestedArray::getValue($values, ['table', $this->uuid(), 'form', 'custom_size_width']);
        $custom_size_height = NestedArray::getValue($values, ['table', $this->uuid(), 'form', 'custom_size_height']);
        if (empty($custom_size_height)|| empty($custom_size_width)) {
          $form_state->setError($form['widgets']['table'][$this->uuid()]['form']['download_size'], $this->t('Width and height have to be specified, if custom is choosen.'));
        }
      }
    }
    catch (\Exception $e) {
      $form_state->setError($form['widgets']['table'][$this->uuid()]['form']['api_key'], $e->getMessage());
    }
  }

  /**
   * {@inheritdoc}
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state) {
    parent::submitConfigurationForm($form, $form_state);
    $values = $form_state->getValues();
    $media_type_values = NestedArray::getValue($values, ['table', $this->uuid(), 'form', 'media_type_container']);

    if (!empty($media_type_values)) {
      foreach ($media_type_values as $key => $value) {
        if (array_key_exists($key, $this->configuration)) {
          $this->configuration[$key] = $value;
        }
      }
    }

    if (substr($this->configuration['upload_location'], -1) != '/') {
      $this->configuration['upload_location'] .= '/';
    }

    if ($this->configuration['download_size'] !== 'custom') {
      $this->configuration['custom_size_width'] = NULL;
      $this->configuration['custom_size_height'] = NULL;
    }

  }

  /**
   * {@inheritdoc}
   */
  public function getForm(array &$original_form, FormStateInterface $form_state, array $additional_widget_parameters) {
    $form = parent::getForm($original_form, $form_state, $additional_widget_parameters);
    $form['#attached']['library'][] = 'media_pexels/entity_browser';

    $form['search'] = [
      '#type' => 'fieldset',
      '#title' => $this->t('Search'),
      '#attributes' => ['class' => ['media-pexels-search']]
    ];

    // Search.
    $form['search']['keyword'] = [
      '#type' => 'search',
      '#title' => $this->t('Keyword'),
      '#default_value' => $form_state->get('keyword') ?: '',
    ];

    $form['search']['orientation'] = [
      '#type' => 'select',
      '#title' => $this->t('Orientation'),
      '#options' => [
        'landscape' => $this->t('Landscape'),
        'portrait' => $this->t('Portrait'),
        'square' => $this->t('Square'),
      ],
      '#empty_value' => '',
      '#default_value' => $form_state->get('orientation') ?: '',
    ];

    $form['search']['color'] = [
      '#type' => 'select',
      '#title' => $this->t('Color'),
      '#options' => [
        'red' => $this->t('Red'),
        'orange' => $this->t('Orange'),
        'yellow' => $this->t('Yellow'),
        'green' => $this->t('Green'),
        'turquoise' => $this->t('Turquoise'),
        'blue' => $this->t('Blue'),
        'violet' => $this->t('Violet'),
        'pink' => $this->t('Pink'),
        'brown' => $this->t('Brown'),
        'black' => $this->t('Black'),
        'gray' => $this->t('Gray'),
        'white' => $this->t('White'),
      ],
      '#empty_value' => '',
      '#default_value' => $form_state->get('color') ?: '',
    ];

    $form['search']['submit'] = [
      '#type' => 'submit',
      '#name' => 'pexels_keyword',
      '#value' => $this->t('Search'),
      '#submit' => [[$this, 'searchSubmit']],
    ];

    $input = $form_state->getUserInput();
    if (!empty($input['keyword'])) {
      $page = EntityBrowserPagerElement::getCurrentPage($form_state);

      try {
        $results = $this->pexelsClient
          ->setApiKey($this->configuration['api_key'])
          ->search($input['keyword'], $this->defaultPerPage, $page, $input['orientation'] ?: '', $input['color'] ?: '');

        if (!isset($results['contents'])) {
          throw new \Exception('Result contents were empty.');
        }

        $search_results = $results['contents'];
      }
      catch (\Exception $e) {
        $this->messenger()->addError($this->t('There was an error while fetching the results. Please contact the system administrator.'));
        $this->loggerFactory->get('media_pexels')->error($e->getMessage());
        return $form;
      }

      if (isset($search_results['total_results']) && $search_results['total_results'] != 0) {
        $form['results'] = [
          '#type' => 'container',
          '#attributes' => ['class' => ['media-pexels-search-results', 'entities-list']],
        ];

        foreach ($search_results['photos'] as $image) {
          $form['results'][$image['id']] = [
            '#type' => 'container',
            '#attributes' => ['class' => ['media-pexels-image', 'grid-item']],
            'checkbox_wrapper' => [
              '#type' => 'container',
              '#attributes' => ['class' => ['views-field-entity-browser-select']],
              'pexels_image_' . $image['id'] => [
                '#type' => 'checkbox',
                '#return_value' => json_encode($image),
                '#attributes' => ['class' => ['media-pexels-select-checkbox']],
              ],
            ],
            'preview' => [
              '#theme' => 'image',
              '#uri' => $image['src']['medium'],
              '#title' => $this->t('By @photographer', ['@photographer' => $image['photographer']]),
            ],
          ];
        }

        $form['pager'] = [
          '#type' => 'entity_browser_pager',
          '#total_pages' => ceil($search_results['total_results'] / $this->defaultPerPage),
          '#process' => [[get_class($this), 'entityBrowserPagerProcess']],
        ];
      }
      else {
        $form['noresults'] = [
          '#type' => 'html_tag',
          '#tag' => 'div',
          '#attributes' => ['class' => ['media-pexels-no-results']],
          '#value' => $this->t('No results were found.'),
        ];
      }

      $quota['credit'] = $this->t('Photos provided by <a href=":link" target="_blank">Pexels</a>.', [
        ':link' => 'https://pexels.com',
      ]);

      if (isset($results['headers']['X-Ratelimit-Remaining']) && isset($results['headers']['X-Ratelimit-Limit'])) {
        $quota['quota'] = $this->t('Monthly quota @hused of @havail.', [
          '@hused' => $results['headers']['X-Ratelimit-Remaining'][0],
          '@havail' => $results['headers']['X-Ratelimit-Limit'][0],
        ]);
      }

      $form['results']['quota'] = [
        '#type' => 'html_tag',
        '#tag' => 'small',
        '#attributes' => ['class' => ['media-pexels-quota']],
        '#value' => implode(' / ', $quota),
      ];
    }

    return $form;
  }

  /**
   * Search form submit callback.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state object.
   */
  public function searchSubmit(array $form, FormStateInterface $form_state) {
    EntityBrowserPagerElement::setCurrentPage($form_state);
    $form_state->setRebuild();
  }

  /**
   * {@inheritdoc}
   */
  public function prepareEntities(array $form, FormStateInterface $form_state) {
    $entities = [];
    $bundle = $this->getType();
    $media_source = $bundle->getSource();

    $selected_images = $this->getSelected($form_state);
    if (empty($selected_images)) {
      return $entities;
    }

    $destination_directory = $this->token->replace($this->configuration['upload_location']);

    if (!file_exists($destination_directory)) {
      $this->fileSystem->mkdir($destination_directory, NULL, TRUE);
    }

    foreach ($selected_images as $selected_image) {
      $image_data = json_decode($selected_image, TRUE);
      $download_size = $this->configuration['download_size'];

      if ($download_size == 'custom') {
        // There is no 'custom' option, so just use 'tiny'.
        $pathinfo = pathinfo($image_data['src']['tiny']);
        $url_parts = UrlHelper::parse($image_data['src']['tiny']);
        $url_parts['query']['w'] = $this->configuration['custom_size_width'];
        $url_parts['query']['h'] = $this->configuration['custom_size_height'];
        $url_parts['query']['fit'] = 'max';
        $download_url = $url_parts['path'] . '?' . UrlHelper::buildQuery($url_parts['query']);
      }
      else {
        $pathinfo = pathinfo($image_data['src'][$download_size]);
        $download_url = $image_data['src'][$download_size];
      }

      $file_name = strtok($pathinfo['basename'], '?');

      $existing_entity = $this->entityTypeManager->getStorage('media')->getQuery()
        ->accessCheck(FALSE)
        ->condition('bundle', $bundle->id())
        ->condition($media_source->getConfiguration()['source_field'] . '.entity:file.filename', $file_name)
        ->execute();

      if ($existing_entity) {
        $entities[] = $this->entityTypeManager->getStorage('media')->load(reset($existing_entity));
      }
      else {
        try {
          $file_uri = $this->pexelsClient->downloadPexelsFile($download_url, $destination_directory . $file_name, FileExists::Rename);
        }
        catch (RequestException $exception) {
          $this->loggerFactory->get('media_pexels')->error($this->t('Failed to fetch file due to error "%error"', ['%error' => $exception->getMessage()]));
          return FALSE;
        }
        catch (FileException $e) {
          $this->loggerFactory->get('media_pexels')->error($this->t('Failed to save file due to error "%error"', ['%error' => $e->getMessage()]));
        }

        if (!$file_uri) {
          $this->messenger->addError($this->t('There was a problem while downloading the selected file.'));
        }

        /** @var \Drupal\file\FileInterface $file */
        $file = $this->entityTypeManager->getStorage('file')
          ->create([
            'filename' => $file_name,
            'uri' => $file_uri,
            'uid' => $this->currentUser->id(),
            'status' => TRUE,
          ]);
        $file->save();

        $entity_values['bundle'] = $bundle->id();
        $source_field_id = $media_source->getConfiguration()['source_field'];
        $entity_values[$source_field_id]['target_id'] = $file->id();

        if (!empty($this->configuration['license_field'])) {
          $entity_values[$this->configuration['license_field']] = $image_data['url'];
        }

        // Use the Pexels image url to get some basic metadata.
        $pexels_url_parts = parse_url($image_data['url']);
        $title_matches = [];
        preg_match('/(?<=\/photo\/)(.*)(?=\-\d*\/)/', $pexels_url_parts['path'], $title_matches);

        // Add title and alt info to a media.
        if ($title_matches) {
          // If alt is present, is it for the image alt text and media name.
          if (isset($image_data['alt'])) {
            $entity_values[$this->configuration['name_field']] = Unicode::truncate($image_data['alt'], 255);
            $entity_values[$source_field_id]['alt'] = Unicode::truncate($image_data['alt'], 512);
          }
          else {
            // Or use title extracted from a file name.
            $title = str_replace('-', ' ', $title_matches[0]);
            $title = ucfirst($title);
            $entity_values[$this->configuration['name_field']] = Unicode::truncate($title, 255);
            $entity_values[$source_field_id]['alt'] = Unicode::truncate($title, 512);
          }
        }

        $media_entity = $this->entityTypeManager->getStorage('media')->create($entity_values);

        $event = $this->eventDispatcher->dispatch(new PexelsMediaEntityCreateEvent($media_entity, $file, $image_data, $form_state), Events::MEDIA_ENTITY_CREATE);
        $media_entity = $event->getMediaEntity();

        $entities[] = $media_entity;
      }
    }

    $form_state->set('pexels_prepared_entities', $entities);
    return $entities;
  }

  /**
   * {@inheritdoc}
   */
  public function validate(array &$form, FormStateInterface $form_state) {
    $selected_images = $this->getSelected($form_state);
    $triggering_element = $form_state->getTriggeringElement();

    if ($triggering_element['#name'] != 'pexels_keyword' && !$selected_images) {
      $form_state->setError($form, $this->t('No images selected.'));
    }

    parent::validate($form, $form_state);
  }


  /**
   * Get selected files from form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   Form state.
   *
   * @return array
   *   The selected files.
   */
  protected function getSelected(FormStateInterface $form_state): array {
    return array_filter($form_state->getValues(), function ($value, $key) {
      return strpos($key, 'pexels_image_') === 0 && !empty($value);
    }, ARRAY_FILTER_USE_BOTH);
  }

  /**
   * Returns the media type that this widget creates.
   *
   * @return \Drupal\Core\Entity\EntityInterface
   *   Media type.
   *
   * @throws \Drupal\Component\Plugin\Exception\InvalidPluginDefinitionException
   * @throws \Drupal\Component\Plugin\Exception\PluginNotFoundException
   */
  protected function getType() {
    return $this->entityTypeManager
      ->getStorage('media_type')
      ->load($this->configuration['media_type']);
  }

  /**
   * {@inheritdoc}
   */
  public function submit(array &$element, array &$form, FormStateInterface $form_state) {
    /** @var \Drupal\media\MediaInterface[] $media_entities */
    $media_entities = $form_state->get('pexels_prepared_entities');

    foreach ($media_entities as &$media_entity) {
      if ($media_entity->isNew()) {
        $media_entity->save();
      }
    }

    $this->selectEntities($media_entities, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function access() {
    if (!$this->configuration['api_key']) {
      return AccessResult::forbidden();
    }
    return AccessResult::allowedIfHasPermission(\Drupal::currentUser(), 'use media_pexels widget');
  }

  /**
   * Process function for the eb pager.
   */
  public static function entityBrowserPagerProcess(array $element, FormStateInterface $form_state, array $complete_form) {
    EntityBrowserPagerElement::processEntityBrowserPager($element, $form_state, $complete_form);
    $page = EntityBrowserPagerElement::getCurrentPage($form_state);
    $element['current']['#value'] = t('Page @page of @total', [
      '@page' => $page,
      '@total' => $element['#total_pages'],
    ]);

    return $element;
  }

}
