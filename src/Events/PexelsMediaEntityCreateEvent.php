<?php

namespace Drupal\media_pexels\Events;

use Drupal\Component\EventDispatcher\Event;
use Drupal\Core\Form\FormStateInterface;
use Drupal\file\FileInterface;
use Drupal\media\MediaInterface;

/**
 * Fires when a media entity is created.
 */
class PexelsMediaEntityCreateEvent extends Event {

  /**
   * The media entity being created.
   *
   * @var \Drupal\media\MediaInterface
   */
  protected $mediaEntity;

  /**
   * The file that will be used for the media entity.
   *
   * @var \Drupal\file\FileInterface
   */
  protected $file;

  /**
   * Pexels data.
   *
   * @var array
   */
  protected $pexelsData;

  /**
   * The form state.
   *
   * @var \Drupal\Core\Form\FormStateInterface
   */
  protected $formState;

  /**
   * PexelsMediaEntityCreateEvent constructor.
   *
   * @param \Drupal\media\MediaInterface $media_entity
   *   The media entity being created.
   * @param \Drupal\file\FileInterface $file
   *   The file that will be used for the media entity.
   * @param array $pexels_data
   *   The file that will be used for the media entity.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state.
   */
  public function __construct(MediaInterface $media_entity, FileInterface $file, array $pexels_data, FormStateInterface $form_state) {
    $this->mediaEntity = $media_entity;
    $this->file = $file;
    $this->pexelsData = $pexels_data;
    $this->formState = $form_state;
  }

  /**
   * Get the media entity.
   *
   * @return \Drupal\media\MediaInterface
   *   A media entity.
   */
  public function getMediaEntity() {
    return $this->mediaEntity;
  }

  /**
   * Get the file for the media entity.
   *
   * @return \Drupal\file\FileInterface
   *   The file that will be used for the media entity.
   */
  public function getFile() {
    return $this->file;
  }

  /**
   * Set the media entity.
   *
   * @param \Drupal\media\MediaInterface $media_entity
   *   The updated media entity.
   */
  public function setMediaEntity(MediaInterface $media_entity) {
    $this->mediaEntity = $media_entity;
  }

  /**
   * Get the data received from Pexels.
   *
   * @return array
   *   The file that will be used for the media entity.
   */
  public function getPexelsData() {
    return $this->pexelsData;
  }

  /**
   * Get the form state.
   *
   * @return \Drupal\Core\Form\FormStateInterface
   *   The current form state.
   */
  public function getFormState() {
    return $this->formState;
  }

  /**
   * Set the form state.
   *
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The updated form state.
   */
  public function setFormState(FormStateInterface $form_state) {
    $this->formState = $form_state;
  }

}
