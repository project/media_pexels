<?php

namespace Drupal\media_pexels\Events;

/**
 * Contains all events thrown by Pexels.
 */
final class Events {

  /**
   * The MEDIA_ENTITY_CREATE event.
   *
   * The MEDIA_ENTITY_CREATE event occurs when creating a new Media Entity.
   *
   * @var string
   */
  const MEDIA_ENTITY_CREATE = 'media_pexels.media_entity_create';

}
