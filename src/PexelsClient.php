<?php

namespace Drupal\media_pexels;

use Drupal\Core\File\FileSystemInterface;
use GuzzleHttp\Client;
use GuzzleHttp\Exception\ConnectException;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\CurlHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Middleware;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;

/**
 * Provides integration with Pexels api.
 */
class PexelsClient {

  const API_URL = 'https://api.pexels.com/v1/';
  const RETRY_TIMES = 2;
  const RETRY_DELAY = 1500;

  /**
   * The http client.
   *
   * @var \GuzzleHttp\Client
   */
  protected $httpClient;

  /**
   * The api key.
   *
   * @var string
   */
  protected $apiKey;

  /**
   * File system.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  private $fileSystem;

  /**
   * PexelsClient constructor.
   *
   * @param \GuzzleHttp\Client $http_client
   *   Http client service.
   * @param \Drupal\Core\File\FileSystemInterface $file_system
   *   File system service.
   */
  public function __construct(Client $http_client, FileSystemInterface $file_system) {
    $this->fileSystem = $file_system;
    $this->httpClient = $http_client;
  }

  /**
   * Calls Pexels api and handles the response.
   *
   * @param string $url
   *   The full enpdoint ur.
   *
   * @return array
   *   Array with headers and contents elements.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function doRequest(string $url): array {
    $handler_stack = HandlerStack::create(new CurlHandler());
    $handler_stack->push(Middleware::retry($this->retryDecider(), $this->retryDelay()));
    $http_client = new Client(['handler' => $handler_stack]);

    $result = $http_client
      ->request('GET', $url, [
          'headers' => [
            'Authorization' => $this->getApiKey(),
          ]]
      );

    return [
      'headers' => $result->getHeaders(),
      'contents' => json_decode($result->getBody()->getContents(), TRUE),
    ];
  }

  /**
   * Checks the validity of an api key.
   *
   * @param string $api_key
   *   The api key.
   *
   * @return bool
   *   True if the test request was successful, false otherwise.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function checkKey(string $api_key): bool {
    $this->setApiKey($api_key);
    $result = $this->doRequest(static::API_URL . 'search?query=people');
    return !empty($result);
  }

  /**
   * Set the auth key.
   *
   * @param string $api_key
   *   The api key.
   *
   * @return \Drupal\media_pexels\PexelsClient
   *   The client.
   */
  public function setApiKey(string $api_key): PexelsClient {
    $this->apiKey = $api_key;
    return $this;
  }

  /**
   * Get the auth key.
   *
   * @return string
   *   The api key.
   *
   * @throws \Exception
   *   If no key was set.
   */
  public function getApiKey(): string {
    if (!$this->apiKey) {
      throw new \Exception('Api key not set.');
    }
    return $this->apiKey;
  }

  /**
   * Search the Pexels image database.
   *
   * @param string $query
   *   The searched query.
   * @param int $size
   *   How many images to return.
   * @param int $page
   *   Which page of the results to get.
   * @param string $orientation
   *   (optional) Orientation options are: landscape, portrait or square.
   * @param string $color
   *   (optional( Color options are: red, orange, yellow, green, turquoise,
   *   blue, violet, pink, brown, black, gray, white or any hexidecimal color
   *   code (eg. #ffffff).
   *
   * @return array
   *   Array with headers and contents elements or empty array if something
   *   went wrong.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function search(string $query, int $size = 15, int $page = 1, $orientation = '', $color = ''): array {
    $query_args = [
      'query' => $query,
      'per_page' => $size,
      'page' => $page
    ];

    if ($orientation) {
      $query_args['orientation'] = $orientation;
    }
    if ($color) {
      $query_args['color'] = $color;
    }

    return $this->doRequest(self::API_URL . 'search?' . http_build_query($query_args));
  }

  /**
   * Get a Pexels photo.
   *
   * @param int $id
   *   The photo id to fetch.
   *
   * @return array
   *   Array with headers and contents elements or empty array if something
   *   went wrong.
   *
   * @throws \GuzzleHttp\Exception\GuzzleException
   */
  public function getPhoto(int $id): array {
    return $this->doRequest(self::API_URL . 'photos/' . $id);
  }

  /**
   * Download a file from pexels.
   *
   * This is mostly a copy from system_retrieve_file.
   *
   * @param string $url
   *   The URL of the file to grab.
   * @param string $destination
   *   Stream wrapper URI specifying where the file should be placed. If a
   *   directory path is provided, the file is saved into that directory under
   *   its original name. If the path contains a filename as well, that one will
   *   be used instead.
   * @param int $replace
   *   Replace behavior when the destination file already exists:
   *   - FileSystemInterface::EXISTS_REPLACE: Replace the existing file.
   *   - FileSystemInterface::EXISTS_RENAME: Append _{incrementing number} until
   *     the filename is unique.
   *   - FileSystemInterface::EXISTS_ERROR: Do nothing and return FALSE.
   *
   * @return \Drupal\file\FileInterface|false|mixed
   *   A string with the path of the resulting file, or FALSE on error.
   */
  public function downloadPexelsFile($url, $destination, $replace = TRUE) {
    $parsed_url = parse_url($url);

    if (is_dir($this->fileSystem->realpath($destination))) {
      // Prevent URIs with triple slashes when glueing parts together.
      $path = str_replace('///', '//', "$destination/") . $this->fileSystem->basename($parsed_url['path']);
    }
    else {
      $path = $destination;
    }

    $data = (string) $this->httpClient
      ->get($url)
      ->getBody();
    return $this->fileSystem->saveData($data, $path, $replace);
  }


  /**
   * The retry decinder.
   *
   * @return \Closure
   *   The retry decider.
   */
  public function retryDecider(): \Closure {
    return function ($retries, Request $request, Response $response = NULL, RequestException $exception = NULL) {
      if ($retries >= self::RETRY_TIMES) {
        return FALSE;
      }

      // Retry connection exceptions.
      if ($exception instanceof ConnectException) {
        return TRUE;
      }
      elseif ($exception instanceof RequestException) {
        return TRUE;
      }

      if ($response) {
        // Retry on server errors.
        if ($response->getStatusCode() >= 500) {
          return TRUE;
        }
      }

      return FALSE;
    };
  }

  /**
   * The retry delay.
   *
   * @return \Closure
   *   The retry delay.
   */
  public function retryDelay(): \Closure {
    return function ($number_of_retries) {
      return self::RETRY_DELAY;
    };
  }

}
